# README #

### Mini Markup Source Files ###

minimarkup.py helps you make HTML or XML using raw python

There are several variants

### How do I get set up? ###

Just drop minimarkup.py in your python path or 

```
ln -sf ../pathto/minimarkup minimarkup
```

### Use ###
```
#!python
from minimarkup import P,Q,P0

with p.head():
   p.h3('Title')
```

Example using bootstrap 3
```
#!python
import os, platform, cherrypy
from minimarkup.bootstrap3 import Page

class View(Page):

    def login(self):
        with self.div(class_='container'):
            with self.form(class_='form-signin'):
                self.h2('Please sign in', class_='form-signin-heading')
                self.label('Email address', for_="inputEmail", class_="sr-only")
                self.input(None, type="email", id="inputEmail", class_="form-control", placeholder="Email address", required=True, autofocus=True)
                self.label('Password', for_="inputPassword", class_="sr-only")
                self.input(None, type="password", id="inputPassword", class_="form-control", placeholder="Password", required=True)
                with self.div(class_="checkbox"):
                    with self.label():
                        self.input('Remember me', type="checkbox", value="remember-me")
                self.button('Sign in', class_="btn btn-lg btn-primary btn-block", type="submit")
        return self.render()

class Login(object):

    @cherrypy.expose
    def index(self):
        return View(css=['signin']).login()
```