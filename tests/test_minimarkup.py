"""
 ~/rhut.py minimarkup.test_minimarkup
"""

from minimarkup.minimarkup import P, Q, P0


def debug(actual, expected):
    """
    """
    if actual == expected:
        return
    print(f'found   :{actual}\nexpected:{expected}')
    assert actual == expected


class Html5(P):
    """
    """
    NOCLOSE = ['img', 'link', 'meta']
    TAGS = NOCLOSE + ['body', 'head', 'html', 'script', 'a']


def test_html():
    p = Html5('<!html>')
    p.html()
    p.head()
    p.meta(funny_one='content')
    p.link(class_='link.src')
    p.script(src='file.js').close()
    p.head.close()
    p.body()
    p.a(id_='123').noclose()
    p.body.close()
    p.html.close()
    assert p() == '<!html>\n<html>\n\t<head>\n\t\t<meta funny-one="content">\n\t\t<link class="link.src">\n\t\t<script src="file.js">\n\t\t</script>\n\t</head>\n\t<body>\n\t\t<a id="123">\n\t</body>\n</html>', p()


def test_html_with():
    """
    """
    p = Html5('<!html>')
    with p.html():
        with p.head():
            p.meta(funny_one='content')
            p.link(class_='link.src')
            p.script(src='file.js').close()
        with p.body():
            p.a(id_='123').noclose()
    assert p() == '<!html>\n<html>\n\t<head>\n\t\t<meta funny-one="content">\n\t\t<link class="link.src">\n\t\t<script src="file.js">\n\t\t</script>\n\t</head>\n\t<body>\n\t\t<a id="123">\n\t</body>\n</html>', p()


def test_P():
    """
    """
    p = P()
    p.data()
    p.list(elements=10)
    for j in range(10):
        p.row(index=j, data=j * j).embedclose()
    p.list.close()
    p.list(elements=10)
    for j in range(10):
        p.row('f', index=j)
    p.list.close()
    p.data.close()
    assert p() == '<data>\n\t<list elements="10">\n\t\t<row index="0" data="0"/>\n\t\t<row index="1" data="1"/>\n\t\t<row index="2" data="4"/>\n\t\t<row index="3" data="9"/>\n\t\t<row index="4" data="16"/>\n\t\t<row index="5" data="25"/>\n\t\t<row index="6" data="36"/>\n\t\t<row index="7" data="49"/>\n\t\t<row index="8" data="64"/>\n\t\t<row index="9" data="81"/>\n\t</list>\n\t<list elements="10">\n\t\t<row index="0">f</row>\n\t\t<row index="1">f</row>\n\t\t<row index="2">f</row>\n\t\t<row index="3">f</row>\n\t\t<row index="4">f</row>\n\t\t<row index="5">f</row>\n\t\t<row index="6">f</row>\n\t\t<row index="7">f</row>\n\t\t<row index="8">f</row>\n\t\t<row index="9">f</row>\n\t</list>\n</data>', str(p)


def test_P_with():
    """
    """
    p = P()
    with p.data():
        with p.list(elements=10):
            for j in range(10):
                p.row(index=j, data=j * j).embedclose()
        with p.list(elements=10):
            for j in range(10):
                p.row('f', index=j)
    assert p() == '<data>\n\t<list elements="10">\n\t\t<row index="0" data="0"/>\n\t\t<row index="1" data="1"/>\n\t\t<row index="2" data="4"/>\n\t\t<row index="3" data="9"/>\n\t\t<row index="4" data="16"/>\n\t\t<row index="5" data="25"/>\n\t\t<row index="6" data="36"/>\n\t\t<row index="7" data="49"/>\n\t\t<row index="8" data="64"/>\n\t\t<row index="9" data="81"/>\n\t</list>\n\t<list elements="10">\n\t\t<row index="0">f</row>\n\t\t<row index="1">f</row>\n\t\t<row index="2">f</row>\n\t\t<row index="3">f</row>\n\t\t<row index="4">f</row>\n\t\t<row index="5">f</row>\n\t\t<row index="6">f</row>\n\t\t<row index="7">f</row>\n\t\t<row index="8">f</row>\n\t\t<row index="9">f</row>\n\t</list>\n</data>', str(p)


def test_Q():
    """
    """
    assert Q.h1() == '<h1>'
    assert Q.h1.close() == '</h1>'
    assert Q.h1('head') == '<h1>head</h1>'
    assert Q.h1(font='big') == '<h1 font="big">'
    assert Q.h1('head', font='big') == '<h1 font="big">head</h1>'
    assert Q.h1('head', font='big', color='blue', number=9, selected=True) == '<h1 font="big" color="blue" number="9" selected>head</h1>'


def test_xml():
    """
    """
    r = P0('<!xml>')
    r.radar_type()
    r.info(name='dover').embedclose()
    r.timestamp('12/04/1960')
    r.src()
    r.offset(23)
    r.size(100)
    r.src.close()
    r.radar_type.close()
    assert str(r) == r() == '<!xml><radar_type><info name="dover"/><timestamp>12/04/1960</timestamp><src><offset>23</offset><size>100</size></src></radar_type>'


def test_xml_with():
    """
    """
    r = P0('<!xml>')
    with r.radar_type():
        r.info(name='dover').embedclose()
        r.timestamp('12/04/1960')
        with r.src():
            r.offset(23)
            r.size(100)
    assert str(r) == r() == '<!xml><radar_type><info name="dover"/><timestamp>12/04/1960</timestamp><src><offset>23</offset><size>100</size></src></radar_type>'


def test_list():
    """
    """
    assert Q.tag([1, 2, 3]) == '<tag>1</tag><tag>2</tag><tag>3</tag>'
    debug(Q.tag([1, 2, 3], key=[4, 5]), '<tag key="4">1</tag><tag key="5">2</tag><tag key="5">3</tag>')
    debug(Q.tag([1, 2], key=[3, 4, 5], key2=6), '<tag key="3" key2="6">1</tag><tag key="4" key2="6">2</tag><tag key="5" key2="6">2</tag>')


def test_none():
    """
    """
    assert Q.tag(None) == '<tag></tag>'
    assert Q.tag([None], style=['top', 'bottom', 'middle']
                 ) == '<tag style="top"></tag><tag style="bottom"></tag><tag style="middle"></tag>'
