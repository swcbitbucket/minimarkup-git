"""
Interface to Chart.js.
https://www.chartjs.org
"""

import sys
import datetime
import time

from jinja2 import Template

# Fill colours
F_RED = 'rgba(255, 99, 132, 0.2)'
F_BLUE = 'rgba(54, 162, 235, 0.2)'
F_YELLOW = 'rgba(255, 206, 86, 0.2)'
F_GREEN = 'rgba(75, 192, 192, 0.2)'
F_PURPLE = 'rgba(153, 102, 255, 0.2)'
F_ORANGE = 'rgba(255, 159, 64, 0.2)'

# Border colours
B_RED = F_RED.replace('0.2', '1')
B_BLUE = F_BLUE.replace('0.2', '1')
B_YELLOW = F_YELLOW.replace('0.2', '1')
B_GREEN = F_GREEN.replace('0.2', '1')
B_PURPLE = F_PURPLE.replace('0.2', '1')
B_ORANGE = F_ORANGE.replace('0.2', '1')


def chart_parse(dtstr):
    """
    # Accept 2 different string formats to convert to datetime or pass through datetime
    """
    if isinstance(dtstr, datetime.datetime):
        return dtstr
    if ':' in dtstr:
        fmt = '%Y-%m-%d %H:%M:%S'
    else:
        fmt = '%Y%m%d-%H%M%S'
    return datetime.datetime.strptime(dtstr, fmt)


def chart_time(dtstr):
    """
    # Convert a datetime to a javascript date format (milliseconds since 1970)
    """
    try:
        dt = chart_parse(dtstr)
    except ValueError as msg:
        print(msg, dtstr, file=sys.stderr)
        return 0
    return 1000 * int(time.mktime(dt.utctimetuple()))


def chart_value(v):
    """
    # Make sure we have a floating point number
    """
    try:
        return float(v)
    except ValueError as msg:
        print(msg, v, file=sys.stderr)
        return 0.0


def cjs_dt_list(values):
    """
    # Accept a dict key = datetime and value is point to be plotted
    """
    result = []
    for dt, value in sorted(values.items()):
        result.append(dict(x=chart_time(dt), y=chart_value(value)))
    return result


class Methods:
    """
    # Implementations of useful graphs.
    """

    def cjs_css_cdn(self):
        """
        # No css for Chartjs
        """
        pass

    def cjs_css_2(self):
        """
        # No css for Chartjs
        """
        pass

    def cjs_js_cdn(self):
        """
        # Chart js needs moment for conversion between strings and javascript datetimes.
        """
        self.javascript(src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js')
        self.javascript(src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js')

    def cjs_js_2(self):
        """
        # Chart js needs moment for conversion between strings and javascript datetimes.
        """
        self.javascript(src='/static/moment-2.24.0.js')
        self.javascript(src='/static/Chart-2.7.3.min.js')

    def _cjs_wrap(self, script):
        """
        """
        myid = self.divid
        with self.canvas(id_=myid):
            self.document_ready('''
new Chart("%(myid)s", {
%(script)s
})
''' % dict(myid=myid, script=script))
        return myid

    def cjs_jinja(self, script, **kwds):
        """
        """
        return self._cjs_wrap(Template(script).render(**kwds))

    def cjs_pie(self, title, labels, colors, datasets, cutout=0):
        """
        """
        if isinstance(datasets, dict):
            datasets = [datasets]  # Wrap in list
        self.cjs_jinja('''
            type: 'pie',
            data:
            {
                labels: {{ labels }},
                datasets:
                [
                {%- for dataset in datasets %}
                    {
                        backgroundColor: {{ colors }},
                        data:{{ dataset['data'] }},
                    },
                {%- endfor %}
                ]
            },
            options:
            {
                responsive: true,
                maintainAspectRatio: false,
                cutoutPercentage: {{ cutout }},
                title:
                {
                    display: true,
                    text: '{{ title|default('Title Missing') }}',
                },
            },
            ''', title=title, labels=labels, colors=colors, datasets=datasets, cutout=cutout)

    def cjs_timeline(self, title, ytitle, datasets, stacked=False):
        """
        """
        if isinstance(datasets, dict):
            datasets = [datasets]  # Wrap in list
        self.cjs_jinja('''
            type: 'line',
            data:
            {
                datasets:
                [
                {%- for dataset in datasets %}
                    {
                        label: '{{ dataset['label']|default('label') }}',
                        backgroundColor: '{{ dataset['fillcolor']|default('white') }}',
                        borderColor: '{{ dataset['bordercolor']|default('black') }}',
                        borderWidth: {{ dataset['borderwidth']|default(1) }},
                        fill: {{ dataset['fill']|default('true') }},
                        tension: {{ dataset['tension']|default(0.4) }},
                        steppedLine: {{ dataset['stepped']|default('false') }},
                        data:{{ dataset['data'] }}
                    },
                {%- endfor %}
                ]
            },
            options:
            {
                responsive: true,
                maintainAspectRatio: false,
                title:
                {
                    display: true,
                    text: '{{ title }}'
                },
                scales:
                {
                    xAxes:
                    [
                        {
                            type: 'time',
                            display: true,
                        }
                    ],
                    yAxes:
                    [
                        {
                            stacked: {{ stacked|default('false') }},
                            scaleLabel:
                            {
                                display: true,
                                labelString: '{{ ytitle }}'
                            },
                            ticks:
                            {
                                beginAtZero:true
                            }
                        }
                    ]
                }
            }
        ''', title=title, ytitle=ytitle, datasets=datasets, stacked='true' if stacked else 'false')

    def cjs_bar(self, title, ytitle, xlabels, datasets):
        """
        """
        if isinstance(datasets, dict):
            datasets = [datasets]  # Wrap in list
        self.cjs_jinja('''
            type: 'bar',
            data:
            {
                labels: {{ xlabels }},
                datasets:
                [
                {%- for dataset in datasets %}
                    {
                        label: '{{ dataset['label'] }}',
                        data: {{ dataset['data'] }},
                        backgroundColor: '{{ dataset['fillcolor'] }}',
                        borderColor: "{{ dataset['bordercolor'] }}",
                        borderWidth: 1
                    },
                {%- endfor %}
                ]
            },
            options:
            {
                responsive: true,
                maintainAspectRatio: false,
                title:
                {
                    display: true,
                    text: '{{ title }}'
                },
                scales:
                {
                    yAxes:
                    [
                        {
                            scaleLabel:
                            {
                                display: true,
                                labelString: '{{ ytitle }}'
                            },
                            ticks:
                            {
                                beginAtZero:true
                            }
                        }
                    ]
                }
            }
            ''', title=title, ytitle=ytitle, xlabels=xlabels, datasets=datasets)
