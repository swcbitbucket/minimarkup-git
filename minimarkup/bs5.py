"""
"""

from minimarkup.minimarkup import Q


class Methods:
    """
    """

    def icon(self, name, font_size='1rem', color='black'):
        """
        """
        return f'<i class="bi {name}" style="font-size: {font_size}; color: {color};"></i>'

    def bs_css(self):
        """
        """
        self.link(href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css", rel="stylesheet", integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD", crossorigin="anonymous")
        self.link(rel="stylesheet", href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css")
        self.link(rel="stylesheet", href="https://unpkg.com/bootstrap-table@1.21.3/dist/bootstrap-table.min.css")

    def bs_js(self):
        """
        """
        self.javascript(src="https://code.jquery.com/jquery-3.3.1.min.js", integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=", crossorigin="anonymous")
        self.javascript(src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js", integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN", crossorigin="anonymous")
        self.javascript(src="https://unpkg.com/bootstrap-table@1.21.3/dist/bootstrap-table.min.js")
