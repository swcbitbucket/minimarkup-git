"""
"""

import datetime
import json


def json_serial(obj):
    """
    json serializer for objects not serializable by default json code
    """

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


class Methods:
    """
    """

    def vis_js_4(self):
        """
        """
        self.javascript(src='/static/vis-4.21.0/dist/vis.js')

    def vis_css_4(self):
        """
        """
        self.link(rel="stylesheet", href='/static/vis-4.21.0/dist/vis.css')

    def vis_js_cdn(self):
        """
        """
        self.javascript(src='https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.js')

    def vis_css_cdn(self):
        """
        """
        self.link(rel="stylesheet", href='https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.css')

    def vis_timeline(self, data, window_start=None, window_width=datetime.timedelta(minutes=20)):
        """
        data can be either:
        a dictionary of datetime key to value
        {datetime.dateimtime(...) : '<some html>', ...}

        an optional window_start, datetimes[0] is used by default
        window_width datetime.timedelta
        """
        if not window_start:
            window_start = sorted(data.keys())[0]
        endt = window_start + window_width
        options = {'start': window_start, 'end': endt}

        myid = self.divid
        events = [{'id': i, 'start': t, 'content': c} for i, (t, c) in enumerate(data.items())]

        with self.div(id_=myid, style='width=100%;height=100%'):
            self.document_ready('''
            var container = document.getElementById('%(myid)s')
            var items = new vis.DataSet(%(events)s);
            var options = %(options)s;
            var timeline = new vis.Timeline(container, items, options);
        ''' % dict(myid=myid,
                   events=json.dumps(events, default=json_serial),
                   options=json.dumps(options, default=json_serial)))
        return myid
