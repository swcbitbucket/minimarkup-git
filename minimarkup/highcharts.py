"""
"""

import time
import datetime

from jinja2 import Template

DEFAULT = 'white'
PRIMARY = '#1CA8DD'
INFO = '#9F86FF'
DANGER = '#E64759'
SUCCESS = '#1BC98E'
WARNING = '#E4D836'

ONESEC = datetime.timedelta(seconds=1)
DELTA = datetime.timedelta(seconds=10)


def parse(dtstr):
    """
    """
    if isinstance(dtstr, datetime.datetime):
        return dtstr
    if ':' in dtstr:
        fmt = '%Y-%m-%d %H:%M:%S'
    else:
        fmt = '%Y%m%d-%H%M%S'
    return datetime.datetime.strptime(dtstr, fmt)


def _adjust(dtstr, delta):
    """
    """
    return str(parse(dtstr) + delta)


def hc_onoffdata(data):
    """
    """
    result = {}
    last = None
    for dt in sorted(data):
        this = data[dt]
        if this != last:
            if last is not None:
                result[_adjust(dt, -ONESEC)] = last
            result[dt] = this
        last = this
    result[dt] = last
    return result


def hc_bardata(data, delta=None):
    """
    """
    if delta is None:
        delta = DELTA
    result = {}
    for dt in data:
        result[_adjust(dt, -delta)] = 0
        result[_adjust(dt, ONESEC - delta)] = data[dt]
        result[_adjust(dt, delta - ONESEC)] = data[dt]
        result[_adjust(dt, +delta)] = 0
    return result


def hc_time(dtstr):
    """
    """
    try:
        dt = parse(dtstr)
    except ValueError as msg:
        print(msg, dtstr)
    return 1000 * int(time.mktime(dt.utctimetuple()))


def hc_value(v):
    """
    """
    try:
        return float(v)
    except ValueError:
        return v


def hc_time_series(series):
    """
    """
    for d in series:
        data = d['data']
        result = []
        for dt in data:
            result.append([hc_time(dt), hc_value(data[dt])])
        d['data'] = sorted(result)
    return series


def hc_dt_list(values):
    """
    """
    result = []
    for dt, value in values.items():
        result.append([hc_time(dt), hc_value(value)])
    return sorted(result)


def hc_dt_range(values):
    """
    """
    result = []
    for dt, (vmin, vmax) in values.items():
        result.append([hc_time(dt), hc_value(vmin), hc_value(vmax)])
    return sorted(result)


def hc_undefined(what):
    """
    """
    if what is None:
        return 'undefined'
    return what


class Methods:
    """
    """

    def hc_js_6(self):
        """
        """
        self.javascript(src='/static/Highcharts-6/code/highcharts.js')
        self.javascript(src='/static/Highcharts-6/code/highcharts-more.js')
        self.javascript(src='/static/Highcharts-6/code/modules/exporting.js')
        self.javascript(src='/static/Highcharts-6/code/modules/offline-exporting.js')

    def hc_js_7(self):
        """
        """
        self.javascript(src='/static/Highcharts-7/code/highcharts.js')
        self.javascript(src='/static/Highcharts-7/code/highcharts-more.js')
        self.javascript(src='/static/Highcharts-7/code/modules/exporting.js')
        self.javascript(src='/static/Highcharts-7/code/modules/offline-exporting.js')

    def hc_js_4(self):
        """
        """
        self.javascript(src='/static/Highcharts-4-2/js/highcharts.js')
        self.javascript(src='/static/Highcharts-4-2/js/highcharts-more.js')
        self.javascript(src='/static/Highcharts-4-2/js/modules/exporting.js')
        self.javascript(src='/static/Highcharts-4-2/js/modules/offline-exporting.js')

    def hc_js_cdn(self):
        """
        """
        self.javascript(src='https://code.highcharts.com/highcharts.src.js')
        self.javascript(src='https://code.highcharts.com/highcharts-more.src.js')
        self.javascript(src='https://code.highcharts.com/modules/exporting.js')
        self.javascript(src='https://code.highcharts.com/modules/offline-exporting.js')

    def _hc_wrap(self, script):
        """
        """
        myid = self.divid
        with self.div(id_=myid, style='width=100%;height=100%'):
            self.document_ready('''Highcharts.chart('%(myid)s', {
        //exporting:{enabled:false},
        credits:{enabled:false},
        %(script)s})''' % dict(myid=myid, script=script))
        return myid

    def hc_jinja(self, script, **kwds):
        """
        """
        self._hc_wrap(Template(script).render(**kwds))

    def hc_pie(self, color, title, name, data):
        """
        """
        self._hc_wrap('''
        chart: {
            type: 'pie',
            backgroundColor: '%(color)s',
            plotBackgroundColor: '%(color)s',
        },
        title: {
            text: '%(title)s %(name)s',
            align: 'left',
            style: {color:'white', fontSize:'24px', font:''},
            y:22,
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -40,
                    style: {color: 'contrast',fontSize: '20px',textShadow: '0px 0px 0px black'}
                },
            }
        },
        series: [{
            name: '%(name)s',
            innerSize: '50%%',
            data: %(data)s,
        }]
''' % dict(title=title, name=name, data=data, color=color))

    def hc_column(self, color, title, ytitle, xtitles, series):
        """
        """
        self._hc_wrap('''
        chart: {
            type: 'column',
            backgroundColor: '%(color)s',
        },
        title: {
            text: '%(title)s'
        },
        xAxis: {
            categories: %(xtitles)s
        },
        yAxis: {
            title: {
                text: '%(ytitle)s'
            }
        },
        series: %(series)s
''' % dict(title=title, xtitles=xtitles, ytitle=ytitle, series=series, color=color))

    def hc_areaspline(self, color, title, ytitle, xtitles, series):
        """
        """
        self._hc_wrap('''
        chart: {
            type: 'areaspline',
            backgroundColor: '%(color)s',
        },
        title: {
            text: '%(title)s',
        },
        xAxis: {
            categories: %(xtitles)s,
            startOnTick: false,
        },
        yAxis: {
            title: {
                text: '%(ytitle)s',
            },
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5,
                marker: {
                    enabled: false,
                },
            }
        },
        series: %(series)s
''' % dict(color=color, title=title, ytitle=ytitle, xtitles=xtitles, series=series))

    def hc_timeseries_multi(self, color, title, ytitle, series, max_=None):
        """
        """
        self._hc_wrap('''
        chart: {
            type: 'area',
            backgroundColor: '%(color)s',
            zoomType:'x',
        },
        title: {
            text: '%(title)s'
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            max: %(max_)s,
            title: {
                text: '%(ytitle)s',
            },
        },
        series: %(series)s
''' % dict(color=color, title=title, ytitle=ytitle, series=hc_time_series(series), max_=hc_undefined(max_)))

    def hc_timeseries_single(self, color, title, series, max_=None):
        """
        """
        return self._hc_wrap('''
        chart: {
            type: 'area',
            backgroundColor: '%(color)s',
            zoomType:'x',
        },
        title: {
            text: '%(title)s'
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            max: %(max_)s,
            title: {
                text: '%(ytitle)s'
            }
        },
        legend:{enabled:false},
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {x1: 0,y1: 0,x2: 0,y2: 1},
                    stops: [[0, '%(scolor)s'],[1, 'white']]
                }
            }
        },
        series: %(series)s
''' % dict(color=color, title=title, ytitle=series['name'], series=hc_time_series([series]), max_=hc_undefined(max_), scolor=series['color']))

    def hc_area_range_and_line(self,
        title,         # string
        suffix,        # string
        rangesname,    # string
        ranges,        # [[dt, min, max], ...]
        averagesname,  # string
        averages,      # [[dt, average], ...]
        ):
        """
        """
        self.hc_jinja('''
        title: {
            text: '{{ title }}'
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: null
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: '{{ suffix }}'
        },
        legend: {
        },
        series: [{
            name: '{{ averagesname }}',
            data: {{ averages }},
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        }, {
            name: '{{ rangesname }}',
            data: {{ ranges }},
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.3,
            zIndex: 0,
            marker: {
                enabled: false
            }
            }]''',
        title=title,
        suffix=suffix,
        rangesname=rangesname,
        ranges=hc_dt_range(ranges),
        averagesname=averagesname,
        averages=hc_dt_list(averages)
        )

    def hc_spark(self, series, max_=None, width=None, height=None):
        """
        """
        self._hc_wrap('''
        chart: {
            type: 'area',
            backgroundColor: null,
            borderWidth: 0,
            margin: [2, 2, 2, 2],
            width: %(width)s,
            height: %(height)s,
            style: {
                overflow: 'visible'
            },
        },
        title: {
            text: ''
        },
        xAxis: {
            type:'datetime',
            labels: {
                enabled: false
            },
            title: {
                text: null
            },
            gridLineWidth: 0,
            startOnTick: false,
            endOnTick: false,
            tickPositions: []
        },
        yAxis: {
            max: %(max_)s,
            min: 0,
            labels: {
                enabled: false
            },
            title: {
                text: null
            },
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            startOnTick: false,
            endOnTick: false,
            tickPositions: [0]
        },
        legend: {
            enabled: false
        },
        tooltip: {
            borderWidth: 0,
            hideDelay: 0,
            //formatter: function(){ return this.y + '@' + this.x}
        },
        plotOptions: {
            series: {
                animation: false,
                lineWidth: 1,
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                marker: {
                    radius: 1,
                    states: {
                        hover: {
                            radius: 1
                        }
                    }
                },
                fillOpacity: 0.15
            },
        },
        series: %(series)s
''' % dict(series=hc_time_series([series]), height=hc_undefined(height), width=hc_undefined(width), max_=hc_undefined(max_)))

    def hc_live(self, action, call, ident, wrap=None, refresh=None, title=None, ytitle=None, name=None):
        """
        """
        with self.div(id=f'id{ident}'):
            self.document_ready('''
    chart%(ident)s = Highcharts.chart('id%(ident)s', {
        chart: {
            type: 'areaspline',
        },
        title: {
            text: '%(title)s'
            },
        xAxis: {
            type: 'datetime',
        },
        yAxis: {
            title: {
                text: '%(ytitle)s'
            }
        },
        plotOptions: {
            areaspline: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [[0,'#7cb5ec'], [1,'white']]
                }
            }
        },
        series: [{
            name: '%(name)s',
            data: []
        }]
    })

    function load%(ident)s(){
        $.ajax({
            url: '/json/%(action)s?CALL=%(call)s',
            success: function(data){
                series = chart%(ident)s.series[0]
                series.addPoint(data.result, true, series.data.length>=%(wrap)d)
                yrange = chart%(ident)s.yAxis[0].getExtremes()
                xrange = chart%(ident)s.xAxis[0].getExtremes()
                xmax = Highcharts.dateFormat('%%H:%%M:%%S', xrange.dataMax)
                chart%(ident)s.setTitle(null, {text: 'Points:' + series.data.length + ' Min:' + yrange.dataMin + ' Max:' + yrange.dataMax + ' Current:' + data.result[1] + ' @' + xmax})
                setTimeout(load%(ident)s, 1000 * %(refresh)d)
            },
            cache:false
        })
    }

    setTimeout(load%(ident)s, 500)
''' % dict(ident=ident, action=action, call=call,
    wrap=wrap or 20, title=title or 'Chart Title', ytitle=ytitle or 'Y-Title', name=name or 'Name', refresh=refresh or 20))
