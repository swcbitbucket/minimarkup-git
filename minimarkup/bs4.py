"""
"""

from minimarkup.minimarkup import Q


class Methods:
    """
    """

    def icon(self, name, label=None):
        """
        """
        return Q.span(label, class_=f'fas fa-{name}')

    def bs4_css(self):
        """
        """
        self.link(rel="stylesheet", href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css", integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO", crossorigin="anonymous")
        self.link(rel="stylesheet", href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css", crossorigin="anonymous")
        self.link(rel="stylesheet", href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/css/theme.bootstrap_4.min.css")
        self.link(rel="stylesheet", href="https://use.fontawesome.com/releases/v5.5.0/css/all.css", integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU", crossorigin="anonymous")
        with self.style():
            self += '''
pre {
    background: black;
    color: white;
}

.btn-opaque {
    background:white;
    color:inherit;
}
'''

    def bs4_js(self):
        """
        """
        self.javascript(src="https://code.jquery.com/jquery-3.3.1.min.js", integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=", crossorigin="anonymous")
        self.javascript(src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js", integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49", crossorigin="anonymous")
        self.javascript(src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js", integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy", crossorigin="anonymous")
        self.javascript(src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js", crossorigin="anonymous")
        self.javascript(src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.js")
        self.javascript(src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.1/js/jquery.tablesorter.widgets.js")
        self.javascript_inline("$('.datepicker').datepicker({format: 'dd/mm/yyyy', todayBtn: 'linked'});")
        self.javascript_inline('$("table").tablesorter({theme: "bootstrap"});')
        self.javascript_inline('$(\'[data-toggle="tooltip"]\').tooltip()')
        self.javascript_inline('$(\'[data-toggle="popover"]\').popover()')
