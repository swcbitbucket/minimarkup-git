"""
"""

from minimarkup.minimarkup import Q


class Methods:
    """
    """

    def icon(self, name, label=None):
        """
        """
        return Q.span(label, class_=f'icon icon-{name}')

    def bs3theme_css(self):
        """
        """
        self.link(rel="stylesheet", href="/static/theme-dashboard-3/v3/dist/toolkit-inverse.min.css")
        with self.style():
            self += '''
pre {
    background: black;
    color: white;
}

.btn-opaque {
    background:#252830;
    color:inherit;
}
'''

    def bs3theme_js(self):
        """
        """
        self.javascript(src='/static/jquery-3.3.1.min.js')
        self.javascript(src='/static/popper-1.14.6.min.js')
        self.javascript(src='/static/theme-dashboard-3/v3/dist/toolkit.min.js')
        self.javascript(src='/static/jquery.tablesorter-2.0.5b.js')
        self.javascript_inline('$("table").tablesorter();')
        self.javascript_inline('$(\'[data-toggle="tooltip"]\').tooltip()')
        self.javascript_inline('$(\'[data-toggle="popover"]\').popover()')
