"""
Leaflet map interface.
"""


class Methods:
    """
    """

    def leaflet_wrap_folium_map(self, folium_map, height='400px', width='100%'):
        """
        """
        folium_map._repr_html_()  # make the html
        html_script: str = folium_map.get_root().script.render()
        with self.div(id_=folium_map.get_name(), style=f'width:{width}; height:{height};'):
            self.document_ready(html_script)

    def leaflet_css(self):
        """
        """
        self.link(rel="stylesheet", href="/static/leaflet-1.4.0/leaflet.css")

    def leaflet_js(self):
        """
        """
        self.javascript(src='/static/leaflet-1.4.0/leaflet.js')
        self.javascript(src='/static/Leaflet.GeometryUtil-0.9.1/leaflet.geometryutil.js')
        self.javascript(src='/static/Leaflet.heat-0.2.0/leaflet-heat.js')
        self.javascript(src='/static/Leaflet.ajax-2.1.0/leaflet.ajax.js')

    def leaflet_js_cdn(self):
        """
        """
        self.javascript(src='https://cdn.jsdelivr.net/npm/leaflet@1.4.0/dist/leaflet.js')

    def leaflet_css_cdn(self):
        """
        """
        self.link(rel="stylesheet", href='https://cdn.jsdelivr.net/npm/leaflet@1.4.0/dist/leaflet.css')
