"""
"""

from contextlib import contextmanager

from minimarkup import template
from minimarkup import chartjs
from minimarkup import highcharts
from minimarkup import vis
from minimarkup import leaflet
from minimarkup import bs5

Q = template.Q


class Methods:
    """
    """

    def local_css(self):
        """
        """
        pass

    def local_js(self):
        """
        """
        pass

    def local_table(self, data, header):
        """
        # Data is a list of lists
        # Header is a list of column names
        """
        with self.tablec('table-sm'):
            with self.thead():
                self.tr(Q.th(header))
            with self.tbody():
                for row in data:
                    self.tr(Q.td(row))

    @contextmanager
    def divc(self, classes, **kwd):
        """
        """
        self.div(class_=classes, **kwd)
        yield
        self.div.close()

    @contextmanager
    def tablec(self, classes=None, **kwd):
        """
        """
        if classes is None:
            classes = ''
        self.table(class_=f'table table-bordered {classes}', **kwd)
        yield
        self.table.close()

    def alert(self, msg, severity=None):
        """
        """
        if severity is None:
            severity = 'success'
        with self.divc(f'alert alert-{severity} alert-dismissable', role='alert'):
            self += str(msg)
            with self.button(type_='button', class_='close', data_dismiss='alert'):
                self.span('&times;')

    def reload_div(self, divid, pyscript, refresh):
        """
        """
        self.document_ready('''
function reload_div_%(divid)s()
{
    $("#%(divid)s").load("/load/%(pyscript)s?CALL=reload_%(divid)s #%(divid)s")
    setTimeout(reload_div_%(divid)s, %(refresh)d * 1000)
}

setTimeout(reload_div_%(divid)s, 1 * 1000)
''' % dict(divid=divid, pyscript=pyscript, refresh=refresh))


class Page(Methods, leaflet.Methods, chartjs.Methods, highcharts.Methods, vis.Methods, bs5.Methods, template.Page):
    """
    """

    def __init__(self, *a, **k):
        """
        # NOTE: no css for highcharts
        """
        super().__init__(*a,
                         csslist=[self.bs_css, self.cjs_css_cdn, self.vis_css_cdn, self.leaflet_css_cdn, self.local_css],
                         jslist=[self.bs_js, self.cjs_js_cdn, self.vis_js_cdn, self.hc_js_cdn, self.leaflet_js_cdn, self.local_js],
                         **k)


class Div(Methods, leaflet.Methods, chartjs.Methods, vis.Methods, highcharts.Methods, bs5.Methods, template.Div):
    """
    """
    pass
