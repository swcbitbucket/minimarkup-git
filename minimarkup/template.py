"""
Choose the components of your pages...
"""

from minimarkup.minimarkup import P, Q  # note Q is for importers

NOCLOSE = (  # http://www.w3.org/TR/html5/syntax.html#void-elements
    'area',
    'base',
    'br',
    'col',
    'command',
    'embed',
    'hr',
    'img',
    'input',
    'keygen',
    'link',
    'meta',
    'param',
    'source',
    'track',
    'wbr',
)

TAGS = (
    'a',
    'body', 'button',
    'canvas', 'caption',
    'div',
    'form', 'footer',
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'html', 'header',
    'iframe', 'i',
    'li', 'label',
    'main',
    'nav',
    'option',
    'p', 'pre',
    'script', 'span', 'small', 'select', 'style',
    'title', 'table', 'tr', 'th', 'td', 'thead', 'tbody', 'tfoot',
    'ul',
)


class Helper(P):
    """
    """
    NOCLOSE = NOCLOSE
    TAGS = TAGS + NOCLOSE

    def __init__(self, *args, **kwds):
        """
        """
        super().__init__(*args, **kwds)
        self._js = []
        self._jsinline = []
        self._divid = 0

    def document_ready(self, script):
        """
        """
        self.javascript_inline('''$(function(){
%(script)s
})''' % dict(script=script))

    def javascript(self, **src):
        """
        # eg src=..., integrity=..., crossorigin=... etc
        """
        self._js.append(src)

    def javascript_inline(self, src):
        """
        # inline in a <script> element </script>
        """
        self._jsinline.append(src)

    def _add_javascript(self):
        """
        # Add to end of doc before render
        """
        for src in self._js:
            self.script(None, **src)
        for s in self._jsinline:
            with self.script():
                self += s

    @property
    def divid(self):
        """
        # Make named divs for charts/maps etc.
        """
        self._divid += 1
        return f'divid{self._divid:03d}'

    def render(self):
        """
        """
        self._add_javascript()

        self.body.close()
        self.html.close()
        return self()


class Page(Helper):
    """
    """

    def __init__(self, title=None, body=None, hideonsubmit=True, refresh=None, csslist=None, jslist=None):
        """
        # title is just that
        # body is a dict to pass to self.body()
        # hideonsubmit can be the string to display when working
        # refresh is a number of seconds and an optional redirect string
        # csslist is a list of methods to add css style
        # jslist is a list of methods to add js
        """
        super().__init__('<!doctype html>')
        self.html(lang='en', data_bs_theme="dark")

        with self.head():
            self.meta(charset="utf-8")
            self.meta(name="viewport", content="width=device-width, initial-scale=1, shrink-to-fit=no")
            if refresh is not None:
                self.meta(http_equiv="refresh", content=f"{refresh}")

            if csslist:  # May be None
                for method in csslist:
                    method()

            self.title(title or 'Index')
            # self.style('html, body, main {width: 100%;height: 100%;margin: 0;padding: 0;}')

        if body is None:
            self.body()
        else:
            self.body(**body)

        # When submit pressed show this div and hide elements on the page with class hideonsubmit
        if hideonsubmit:
            if hideonsubmit is True:
                hideonsubmit = 'Working...'
            with self.div(id='waitdiv', style='display:none'):
                self.h3(hideonsubmit)
            self.javascript_inline('''
$("form").submit(function(){
    $(".hideonsubmit").hide()
    $("#waitdiv").show()
})

$(".hideonclick").click(function(){
    $(".hideonsubmit").hide()
    $("#waitdiv").show()
})
''')
        self.javascript_inline('''
function toggle_display(name){
    $('.'+name).toggle()
}
''')

        if jslist:  # May be None
            for method in jslist:
                method()


class Div(Helper):
    """
    # Use this to embed a div in an enclosing page
    """

    def __init__(self, *args, **kwds):
        """
        """
        super().__init__()
        self.div(*args, **kwds)

    def render(self):
        """
        """
        self.div.close()
        self._add_javascript()
        return self()
