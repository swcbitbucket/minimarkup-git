from minimarkup.highcharts import Page, Div

def test1():
    p = Page()
    p.hc_column('white', '1st Quarter', 'Count',
        ['Jan', 'Feb', 'Mar', 'Apr'],
        [dict(name='First',data=[100, 40, 60, 40]),
        dict(name= 'Next',data=[100, 48, 69, 49]),
        dict(name='Second',data=[200,90,30,40])])
    assert p.render()

def test2():
    d = Div()
    d.hc_column('white', '1st Quarter', 'Count',
        ['Jan', 'Feb', 'Mar', 'Apr'],
        [dict(name='First',data=[100, 40, 60, 40]),
        dict(name= 'Next',data=[100, 48, 69, 49]),
        dict(name='Second',data=[200,90,30,40])])
    result = d.render()
    print(result)
    assert result

