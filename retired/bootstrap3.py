import os
from .minimarkup import P,Q

assert os.path.exists('static/bootstrap')

NOCLOSE = (
    'br',
    'hr',
    'img',
    'link',
    'meta',
    )

TAGS = (
    'a',
    'body', 'br', 'button',
    'canvas',
    'div',
    'form', 'footer',
    'h1', 'h2', 'h3', 'h4', 'head', 'hr', 'html',
    'img', 'iframe', 'input',
    'link', 'li', 'label',
    'meta',
    'option',
    'p',
    'script','span','small', 'select',
    'title', 'table', 'tr', 'th', 'td', 'thead', 'tbody',
    'ul',
    )

SPECIAL = (
    'footer',
    'nav',
    )

class Page(P):
    NOCLOSE = NOCLOSE
    TAGS = TAGS+SPECIAL

    def __init__(self, title=None, css=None):
        if title is None:
            title='Index'
        if css is None:
            css=[]
        if isinstance(css, str):
            css = [css]
        assert isinstance(css, list)
        P.__init__(self, '<!DOCTYPE html>')
        self._jsinline = []
        self._js = []
        self.html(lang='en')

        with self.head():
            self.meta (charset="utf-8")
            self.meta (http_equiv="X-UA-Compatible", content="IE=edge")
            self.meta (name="viewport", content="width=device-width, initial-scale=1")
            self.title(title)
            self.link(rel="stylesheet", href="/static/bootstrap/css/bootstrap.min.css")
            for cssname in css:
                fpath = "/static/%s.css" % cssname
                assert os.path.exists(fpath[1:]), fpath
                self.link(rel="stylesheet", href=fpath)

        self.body(class_='with-top-navbar', style='min-height:100%;padding-top:70px')

    def javascript_inline(self, src):
        self._jsinline.append(src)

    def javascript(self, src):
        self._js.append(src)

    def render(self):
        self.script(None, src="/static/jquery.min.js")
        self.script(None, src="/static/bootstrap/js/bootstrap.min.js")
        for src in self._js:
            self.script(None, src=src)
        for s in self._jsinline:
            self.script(s)
        self.body.close()
        self.html.close()
        return self()
