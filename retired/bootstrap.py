from .minimarkup import P,Q

NOCLOSE = (
    'br',
    'hr',
    'img',
    'link',
    'meta',
    )

TAGS = (
    'a',
    'body', 'br', 'button',
    'canvas',
    'div',
    'form',
    'h1', 'h2', 'h3', 'h4', 'head', 'hr', 'html',
    'img', 'iframe', 'input',
    'link', 'li',
    'meta',
    'p',
    'script','span','small',
    'title', 'table', 'tr', 'th', 'td', 'thead', 'tbody',
    'ul',
    )

SPECIAL = (
    'footer',
    'nav',
    )

class Page(P):
    NOCLOSE = NOCLOSE
    TAGS = TAGS+SPECIAL

    def __init__(self, title):
        P.__init__(self, '<!DOCTYPE html>')
        self._jsinline = []
        self._js = []
        self.html(lang='en')

        with self.head():
            self.meta (charset="utf-8")
            self.meta (http_equiv="X-UA-Compatible", content="IE=edge")
            self.meta (name="viewport", content="width=device-width, initial-scale=1")
            self.title(title)
            self.link(rel="stylesheet", href="/static/bootstrap-theme-dashboard/dist/toolkit-inverse.css")
            self.link(rel="stylesheet", href="/static/site.css")

        self.body(class_='with-top-navbar', style='min-height:100%;padding-top:50px')

    def bs_spark(self, type_, title, data_values):
        if not isinstance(data_values[0], list):
            data_values = [data_values]
        mlen = None
        for dv in data_values:
            if mlen is None:
                mlen = len(dv)
                mmax = max(dv)
                mmin = min(dv)
            else:
                assert mlen == len(dv)
                mmax=max(mmax, max(dv))
                mmin=min(mmin, min(dv))
        with self.div(class_='statcard statcard-%s' % type_):
            with self.div(class_='p-a'):
                self.h3('%s min=%s max=%s' % (title, mmin, mmax), class_='statcard-number')
                self.hr(class_='statcard-hr m-b-0')
            self.canvas(None,
                        class_='sparkline',
                        data_chart='spark-line',
                        data_value=[dict(data=_) for _ in data_values],
                        data_labels=['' for _ in range(mlen)])

    def bs_doughnut(self, type_, title, *data):
        with self.div(class_='statcard statcard-%s' % type_):
            with self.div(class_='p-a'):
                self.h3(title, class_='statcard-number')
                self.hr(class_='statcard-hr m-b-0')
            self.canvas(None,
                class_='ex-graph',
                data_chart='doughnut',
                data_value=[dict(value=_[1], label=_[0], color=_[2]) for _ in data])

    def bs_bar(self, data_labels, data_value):
        self.canvas(None,
            class_='ex-line-graph',
            data_chart='bar',
            data_scale_line_color='transparent',
            data_scale_grid_line_color='gray',
            data_scale_font_color='white',
            data_scale_override='false',
            data_labels=data_labels,
            data_value=data_value)

    def bs_line(self, data_labels, data_value):
        self.canvas(None,
            class_='ex-line-graph',
            data_chart='line',
            data_scale_line_color='transparent',
            data_scale_grid_line_color='gray',
            data_scale_font_color='white',
            data_scale_override='false',
            data_labels=data_labels,
            data_value=data_value)

    def javascript_inline(self, src):
        self._jsinline.append(src)

    def javascript(self, src):
        self._js.append(src)

    def render(self):
        self.script(None, src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js")
        self.script(None, src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js")
        self.script(None, src="/static/bootstrap-theme-dashboard/js/custom/chartjs-data-api.js")
        self.script(None, src="/static/bootstrap-theme-dashboard/dist/toolkit.min.js")
        self.script(None, src="/static/jquery.tablesorter.js")
        for src in self._js:
            self.script(None, src=src)
        self.script(None, src="/static/site.js")
        for s in self._jsinline:
            self.script(s)
        self.body.close()
        self.html.close()
        return self()
