"""
Adapted from Html5 Boilerplate.
"""
from .minimarkup import P, Q

OUTDATED = Q.strong('outdated')
UPGRADE = Q.a('upgrade your browser' , href="http://browsehappy.com/")

class Html5(P):
    NOCLOSE = 'br', 'hr', 'img', 'link', 'meta'
    TAGS = ('a', 'body', 'br', 'button', 'div', 'form',
    'h1', 'h2', 'h3', 'h4', 'head', 'hr', 'html',
    'img', 'link', 'meta', 'p', 'script',
    'title', 'table', 'tr', 'th', 'td')

    def __init__(self, title):
        P.__init__(self, '<!DOCTYPE html>')
        self.html(class_='no_js', lang='')

        self.head()
        self.meta (charset="utf-8")
        self.meta (http_equiv="X-UA-Compatible", content="IE=edge")
        self.title(title)
        self.meta(name="description", content="")
        self.meta(name="viewport", content="width=device-width, initial-scale=1")
        self.link(rel='apple-touch-icon', href='/static/img/apple-touch-icon.png')
        self.link(rel="stylesheet", href="/static/css/normalize.css")
        self.link(rel="stylesheet", href="/static/css/main.css")
        self.script(src="/static/js/vendor/modernizr-2.8.3.min.js").close()
        self.head.close()

        self.body()
        self += '<!--[if lt IE 8]>'
        self.p('You are using an %s browser.\n\t\tPlease %s to improve your experience.' % (OUTDATED, UPGRADE), class_="browsehappy")
        self += '<![endif]-->'

    def js(self):
        self.script(src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js").close()
        self.script("window.jQuery || document.write('%s')" % r'<script src="/static/js/vendor/jquery-1.11.2.min.js"><\/script>')
        self.script(src="/static/js/plugins.js").close()
        self.script(src="/static/js/main.js").close()

    def render(self):
        self.body.close()
        self.html.close()
        return self()
