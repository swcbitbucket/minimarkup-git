from .minimarkup import P, Q

NOCLOSE = (
    'br',
    'hr',
    'img',
    'link',
    'meta',
)

TAGS = (
    'a',
    'body', 'br', 'button',
    'canvas', 'caption',
    'div',
    'form', 'footer',
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'hr', 'html',
    'img', 'iframe', 'input',
    'link', 'li', 'label',
    'meta', 'main',
    'nav',
    'option',
    'p', 'pre',
    'script', 'span', 'small', 'select', 'style',
    'title', 'table', 'tr', 'th', 'td', 'thead', 'tbody',
    'ul',
)


class Helper(P):
    NOCLOSE = NOCLOSE
    TAGS = TAGS

    def __init__(self, *args, **kwds):
        super().__init__(*args, **kwds)
        self._js = []
        self._jsinline = []
        self._divid = 0

    def document_ready(self, script):
        self.javascript_inline('''
$(function() {
%(script)s
})
''' % dict(script=script))

    def javascript(self, **src):
        # eg src=..., integrity=..., crossorigin=... etc
        self._js.append(src)

    def javascript_inline(self, src):
        # inline in a <script> element </script>
        self._jsinline.append(src)

    def _add_javascript(self):
        # Add to end of doc before render
        for src in self._js:
            self.script(None, **src)
        for s in self._jsinline:
            with self.script():
                self += s

    @property
    def divid(self):
        # Make named divs for charts/maps etc.
        self._divid += 1
        return f'divid{self._divid:03d}'

    def render(self):
        self._add_javascript()

        self.body.close()
        self.html.close()
        return self()

class Div(Helper):
    # Use this to embed a div in an enclosing page

    def __init__(self, *args, **kwds):
        super().__init__()
        self.div(*args, **kwds)

    def render(self):
        self.div.close()
        self._add_javascript()
        return self()