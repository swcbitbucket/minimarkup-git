from .helper import Q, Div, Helper

def icon(name, label=None):
    return Q.span(label, class_=f'icon icon-{name}')

class Page(Helper):

    def __init__(self, title=None, body=None, hideonsubmit=True, refresh=None):
        # hideonsubmit can be the string to display when working
        super().__init__('<!DOCTYPE html>')
        self.html(lang='en')

        with self.head():
            self.meta(charset="utf-8")
            self.meta(name="viewport", content="width=device-width, initial-scale=1, shrink-to-fit=no")
            if refresh is not None:
                self.meta(http_equiv="refresh", content=f"{refresh}")
            self.link(rel="stylesheet", href="/static/theme-dashboard-4/v4/dist/toolkit-inverse.min.css")
            with self.style():
                self += '''
pre {
    background: black;
    color: white;
}

.btn-opaque {
    background:#252830;
    color:inherit;
}
'''
            self.title(title or 'Index')

        if body is None:
            self.body()
        else:
            self.body(**body)

        # When submit pressed show this div and hide elements on the page with class hideonsubmit
        if hideonsubmit:
            if hideonsubmit is True:
                hideonsubmit = 'Working...'
            with self.div(id='waitdiv', style='display:none'):
                self.h3(hideonsubmit)
            self.javascript_inline('''
$("form").submit(function(){
    $(".hideonsubmit").hide();
    $("#waitdiv").show()
})

$( ".hideonclick" ).click(function() {
    $(".hideonsubmit").hide();
    $("#waitdiv").show()
});

''')

    def render(self):
        self.javascript_inline('$("table").tablesorter();')
        self.javascript_inline('$(\'[data-toggle="tooltip"]\').tooltip()')
        self.javascript_inline('$(\'[data-toggle="popover"]\').popover()')
        self.script(None, src="/static/jquery-3.3.1.min.js")
        self.script(None, src="/static/popper-1.14.6.min.js")
        self.script(None, src='/static/theme-dashboard-4/v4/dist/toolkit.min.js')
        self.script(None, src='/static/jquery.tablesorter-2.0.5b.js')
        self.add_javascript()

        self.body.close()
        self.html.close()
        return self()
